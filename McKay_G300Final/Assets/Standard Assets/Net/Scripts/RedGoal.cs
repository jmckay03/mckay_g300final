﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedGoal : MonoBehaviour {
	
	public Transform puckDrop;
	public Vector3 position;

	void OnTriggerEnter(Collider other) {

		if (other.tag == "Puck") {

			AudioSource audio = GetComponent<AudioSource> ();
			audio.Play ();
			print ("GOAL!");
			RedScore.redScore += 1;
			Instantiate (puckDrop, position, Quaternion.identity);
			Destroy (other.gameObject);

		}

	}
}
