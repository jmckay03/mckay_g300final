﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RedScore : MonoBehaviour {

	public static int redScore = 0;

	Text redText;

	// Use this for initialization
	void Awake () {

		redText = GetComponent<Text> ();
		redScore = 0;


	}

	// Update is called once per frame
	void Update () {

		redText.text = redScore.ToString("D");

	}
}
