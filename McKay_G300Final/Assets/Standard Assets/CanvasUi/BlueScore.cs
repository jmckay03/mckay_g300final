﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BlueScore : MonoBehaviour {

	public static int blueScore = 0;

	Text blueText;

	// Use this for initialization
	void Awake () {

		blueText = GetComponent<Text> ();
		blueScore = 0;


	}

	// Update is called once per frame
	void Update () {

		blueText.text = blueScore.ToString("D");

	}
}
