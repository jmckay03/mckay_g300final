﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PeriodManager : MonoBehaviour {

	public static int period;

	Text periodText;

	// Use this for initialization
	void Awake () {

		periodText = GetComponent<Text> ();
		period = 1;


	}

	// Update is called once per frame
	void Update () {

		periodText.text = "P" + period;

		if (period > 3) {

			period = 1;
			RedScore.redScore = 0;
			BlueScore.blueScore = 0;

		}

	}
}
