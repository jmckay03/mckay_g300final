﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimeManager : MonoBehaviour {

	public static float time;
	public static string formatTime;
	private static float delay = 3.0f;

	Text timeText;

	// Use this for initialization
	void Awake () {

		timeText = GetComponent<Text> ();
		time = 300.0f;


	}

	// Update is called once per frame
	void Update () {
		
		time -= Time.deltaTime;
		timeText.text = time.ToString("F2");

		if (time <= 0f) {
			time = 0f;
			TimeReset ();
		}

	}

	void TimeReset() {
		
		AudioSource audio = GetComponent<AudioSource> ();
		audio.Play ();
		time = 300f;
		PeriodManager.period += 1;


	}


}
