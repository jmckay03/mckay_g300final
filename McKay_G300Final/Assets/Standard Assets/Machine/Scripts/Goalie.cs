﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goalie : MonoBehaviour {

	private Vector3 pos1 = new Vector3(-1.5f, -0.276f, 9.5f);
	private Vector3 pos2 = new Vector3(1.5f, -0.276f, 9.5f);
	public float speed = 1.0f;
	public Transform puckDrop;
	public Vector3 position;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		transform.position = Vector3.Lerp (pos1, pos2, Mathf.PingPong (Time.time * speed, 1.0f));
		
	}

	void OnTriggerEnter(Collider other) {

		if (other.tag == "Puck") {

			AudioSource audio = GetComponent<AudioSource> ();
			audio.Play ();
			print ("SAVE!");
			BlueScore.blueScore += 1;
			Instantiate (puckDrop, position, Quaternion.identity);
			Destroy (other.gameObject);

		}

}
}

