﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickMovement : MonoBehaviour {

	float movedownY = 0f;
	float movedownX = 0f;
	float sensitivityY = 2f;
	float sensitivityX = 100f;
    float swing = 5f;
    float raise = 15f;
    HingeJoint hj;
    Rigidbody rb;


	// Use this for initialization
	void Start () {

        rb = GetComponent<Rigidbody>();
        hj = GetComponent<HingeJoint>();

	}
	
	// Update is called once per frame
	void Update () {



		movedownY += Input.GetAxis ("Mouse Y") * sensitivityY;

		if (Input.GetAxis ("Mouse Y") != 0) {

            float yAnchor = hj.anchor.y;
            yAnchor += -movedownY * Time.deltaTime * raise;
            yAnchor = Mathf.Clamp(yAnchor, -10f, 1f);
            hj.anchor = new Vector3(0f, yAnchor, 0f);

        }
			

		movedownX += Input.GetAxis ("Mouse X") * sensitivityX;

		if (Input.GetAxis ("Mouse X") != 0) {

            rb.angularVelocity += Vector3.up * movedownX * Time.deltaTime * swing;

		}
			

		movedownY = 0f;
		movedownX = 0f;


	}
}
